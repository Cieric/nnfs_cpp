#pragma once

struct Range
{
	int stop, start, step;
	Range(int range) {
		start = 0;
		stop = range;
		step = 1;
	}

	Range(int _start, int _stop, int _step = 1) {
		start = _start;
		stop = _stop;
		step = _step;
	}
};