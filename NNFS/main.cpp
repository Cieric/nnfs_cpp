#include <iostream>
#include "Tensor.h"
#include "DenseLayer.h"
#include "ActivationReLU.h"
#include "nnfs.h"

int main(int argc, char** argv)
{
	//Tensor<double, 3, 4> input = { {1, 2, 3, 2.5},{2., 5., -1., 2},{-1.5, 2.7, 3.3, -0.8} };
	//Tensor<double, 3, 4> weights = {
	//	{0.2, 0.8, -0.5, 1.0},
	//	{0.5, -0.91, 0.26, -0.5},
	//	{-0.26, -0.27, 0.17, 0.87}

	//};

	//Tensor<double, 3> bias = { 2.0, 3.0, 0.5 };
	//auto layer1_outputs = dot(input, weights) + bias;
	//Tensor<double, 3, 3> weights2 = { {0.1, -0.14, 0.5},{-0.5, 0.12, -0.33},{-0.44, 0.73, -0.13}};
	//Tensor<double, 3> biases2 = { -1, 2, -0.5 };


	//auto layer2_outputs = dot(layer1_outputs, weights2) + biases2;

	//layer2_outputs.print();

	auto spiral = spiral_data<100, 3>();
	LayerDense<double, 2, 3> dense1;
	ActivationReLU<double, 3> activation1;
	auto layer1 = activation1.forward(dense1.Forward(spiral.first));
	layer1.print();

	//printf("%lf %lf %lf", test(0), test(1), test(2));
	//weight.print();
	//std::cout << std::endl;
	//test.print();
	return 0;
}