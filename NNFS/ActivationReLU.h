#pragma once
#include <cstddef>
#include "Tensor.h"

template<typename T, std::size_t n_inputs>
class ActivationReLU
{
public:
	Tensor<T, n_inputs> forward(Tensor<T, n_inputs> inputs)
	{
		Tensor<T, n_inputs> results;
		for (int i = 0; i < n_inputs; i++)
			results(i) = (inputs(i) > 0.0) * inputs(i);
		return results;
	}

	template<std::size_t samples>
	Tensor<T, samples, n_inputs> forward(Tensor<T, samples, n_inputs> inputs)
	{
		Tensor<T, samples, n_inputs> results;
		for (int i = 0; i < samples; i++)
			results(i) = forward(inputs(i));
		return results;
	}
};