#pragma once
#include <stdint.h>
#include <cstddef>
#include <initializer_list>
#include <random>
#include "range.h"

template < typename T, std::size_t... >
struct Tensor;

template < typename T, std::size_t... >
struct RangedTensor;

template < typename T, std::size_t Dim1 >
class Tensor<T, Dim1>
{
	T data[Dim1];
public:
	Tensor() {};
	Tensor(std::initializer_list<T> list) {
		int i = 0;
		for (T elem : list)
			data[i++] = elem;
	}

	template <typename Last>
	auto& operator()(Last l) {
		return data[l];
	}

	Tensor<T, Dim1> operator+(Tensor<T, Dim1> rhs) const
	{
		Tensor<T, Dim1> result;
		for (int i = 0; i < Dim1; i++)
			result(i) = data[i] + rhs(i);
		return result;
	}

	Tensor<T, Dim1> operator*(float rhs) const
	{
		Tensor<T, Dim1> result;
		for (int i = 0; i < Dim1; i++)
			result(i) = data[i] * rhs;
		return result;
	}

	Tensor<T, Dim1> operator*(Tensor<T, Dim1> rhs) const
	{
		Tensor<T, Dim1> result;
		for (int i = 0; i < Dim1; i++)
			result(i) = data[i] * rhs(i);
		return result;
	}

	RangedTensor<T, Dim1> operator[](Range range) {
		return RangedTensor<T, Dim1>(*this, range);
	}

	void print() const {
		std::cout << "[";
		int i = 0;
		do {
			std::cout << data[i];
		} while(++i < Dim1 && (std::cout << ", ", 1));
		std::cout << "]";
	}
};

template < typename T, std::size_t Dim1 >
class RangedTensor<T, Dim1>
{
	Tensor<T, Dim1>& tensor;
	Range range;
public:
	RangedTensor(Tensor<T, Dim1>& tensor, Range range) : tensor(tensor), range(range){}

	template<std::size_t DimN>
	void operator=(Tensor<T, DimN> value)
	{
		for (int i = 0; i < DimN; i++)
			tensor(range.start + i) = value(i);
	}

	void operator=(T value)
	{
		for (int i = range.start; i < range.stop; i += range.step)
			tensor(i) = value;
	}
};

template < typename T, std::size_t Dim1, std::size_t... DimN >
class Tensor<T, Dim1, DimN...>
{
	using _Ty = Tensor<T, DimN...>;
	_Ty data[Dim1];
public:
	Tensor() {};
	Tensor(std::initializer_list<_Ty> list) {
		int i = 0;
		for (_Ty elem : list)
			data[i++] = elem;
	}

	template <typename Last>
	auto& operator()(Last l) {
		return data[l];
	}

	template <typename First, typename Second, typename ...Rest>
	auto& operator()(First f, Second s, Rest... r) {
		return data[f](s, r...);
	}

	template<std::size_t DimX>
	Tensor<T, Dim1, DimN...> operator+(Tensor<T, DimX> rhs)
	{
		Tensor<T, Dim1, DimN...> result;
		for (int i = 0; i < Dim1; i++)
			result(i) = data[i] + rhs;
		return result;
	}

	Tensor<T, Dim1, DimN...> operator*(float rhs) const
	{
		Tensor<T, Dim1, DimN...> result;
		for (int i = 0; i < Dim1; i++)
			result(i) = data[i] * rhs;
		return result;
	}

	RangedTensor<T, Dim1, DimN...> operator[](Range range) {
		return RangedTensor<T, Dim1, DimN...>(*this, range);
	}

	void print() const {
		std::cout << "[";
		int i = 0;
		do {
			data[i].print();
		} while (++i < Dim1 && (std::cout << "," << std::endl, 1));
		std::cout << "]";
	}
};

template < typename T, std::size_t Dim1, std::size_t... DimN >
class RangedTensor<T, Dim1, DimN...>
{
	Tensor<T, Dim1, DimN...>& tensor;
	Range range;
public:
	RangedTensor(Tensor<T, Dim1, DimN...>& tensor, Range range) : tensor(tensor), range(range) {}

	template<std::size_t DimX, std::size_t... DimY>
	void operator=(Tensor<T, DimX, DimY...> value)
	{
		for (int i = 0; i < DimX; i++)
			tensor(range.start + i) = value(i);
	}
};

template<typename T, std::size_t S>
T dot(Tensor<T, S> lhs, Tensor<T, S> rhs)
{
	T sum = 0;
	for (int i = 0; i < S; i++)
		sum += lhs(i) * rhs(i);
	return sum;
}

template<typename T, std::size_t M, std::size_t... N, std::size_t S>
Tensor<T, M> dot(Tensor<T, S> lhs, Tensor<T, M, N...> rhs)
{
	Tensor<T, M> result;

	for (int i = 0; i < M; i++)
		result(i) = dot(lhs, rhs(i));

	return result;
}

template<typename T, std::size_t M, std::size_t... N, std::size_t S, std::size_t... U>
Tensor<T, S, M> dot(Tensor<T, S, U...> lhs, Tensor<T, M, N...> rhs)
{
	Tensor<T, S, M> result;
	int j = 0;
	for (; j < S; j++)
		for (int i = 0; i < M; i++)
		{
			auto res = dot(rhs(i), lhs(j));
			result(j)(i) = res;
		}

	return result;
}

template<typename T, std::size_t M, std::size_t N>
Tensor<T, M, N> swap(Tensor<T, N, M> lhs)
{
	Tensor<T, M, N> result = {};
	for (int x = 0; x < M; x++)
		for (int y = 0; y < N; y++)
			result(x, y) = lhs(y, x);
	return result;
}

template<typename T, std::size_t M>
Tensor<T, M> randn()
{
	std::default_random_engine generator;
	std::normal_distribution<T> distribution(0.0, 1.0);
	Tensor<T, M> result = {};
	for (int i = 0; i < M; i++)
		result(i) = distribution(generator);
	return result;
}

template<typename T, std::size_t M, std::size_t N, std::size_t... O>
Tensor<T, M, N, O...> randn()
{
	Tensor<T, M, N, O...> result = {};
	for (int i = 0; i < M; i++)
		result(i) = randn<T, N, O...>();
	return result;
}

template<typename T, std::size_t M>
Tensor<T, M> zeros()
{
	Tensor<T, M> result = {};
	for (int i = 0; i < M; i++)
		result(i) = 0;
	return result;
}

template<typename T, std::size_t M, std::size_t N, std::size_t... O>
Tensor<T, M, N, O...> zeros()
{
	Tensor<T, M, N, O...> result = {};
	for (int i = 0; i < M; i++)
		result(i) = zeros<T, N, O...>();
	return result;
}

template<typename T, std::size_t count>
Tensor<T, count> linspace(T start, T stop)
{
	Tensor<T, count> arr = {};
	for (int i = 0; i < count; i++)
	{
		T step = start + ((stop - start) / count) * i;
		arr(i) = step;
	}
	return arr;
}

template < typename T, std::size_t Dim1>
Tensor<T, Dim1> sin(Tensor<T, Dim1> lhs)
{
	Tensor<T, Dim1> result;
	for (int i = 0; i < Dim1; i++)
		result(i) = sin(lhs(i));
	return result;
}

template < typename T, std::size_t Dim1, std::size_t... DimN >
Tensor<T, Dim1, DimN...> sin(Tensor<T, Dim1, DimN...> lhs)
{
	Tensor<T, Dim1, DimN...> result;
	for (int i = 0; i < Dim1; i++)
		result(i) = sin(lhs(i));
	return result;
}

template < typename T, std::size_t Dim1>
Tensor<T, Dim1> cos(Tensor<T, Dim1> lhs)
{
	Tensor<T, Dim1> result;
	for (int i = 0; i < Dim1; i++)
		result(i) = cos(lhs(i));
	return result;
}

template < typename T, std::size_t Dim1, std::size_t... DimN >
Tensor<T, Dim1, DimN...> cos(Tensor<T, Dim1, DimN...> lhs)
{
	Tensor<T, Dim1, DimN...> result;
	for (int i = 0; i < Dim1; i++)
		result(i) = cos(lhs(i));
	return result;
}