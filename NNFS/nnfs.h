#pragma once
#include "Tensor.h"
#include "range.h"
#include <math.h>

/*
def create_data(samples, classes):
    X = np.zeros((samples*classes, 2))
    y = np.zeros(samples*classes, dtype='uint8')
    for class_number in range(classes):
        ix = range(samples*class_number, samples*(class_number+1))
        r = np.linspace(0.0, 1, samples)
        t = np.linspace(class_number*4, (class_number+1)*4, samples) + np.random.randn(samples)*0.2
        X[ix] = np.c_[r*np.sin(t*2.5), r*np.cos(t*2.5)]
        y[ix] = class_number
    return X, y
*/

template<std::size_t samples, std::size_t classes>
auto spiral_data()
{
    using T = double;

    Tensor<T, samples * classes, 2> X = {};
    Tensor<uint8_t, samples * classes> y = {};
    for (std::size_t class_number = 0; class_number < classes; class_number++)
    {
        auto ix = Range(samples * class_number, samples * (class_number + 1));
        auto r = linspace<T, samples>(0.0, 1);
        auto tempr = randn<T, samples>();
        auto t = linspace<T, samples>(class_number * 4, (class_number + 1) * 4) + tempr * 0.2;
        X[ix] = swap(Tensor<T, 2, 100>{r * sin(t * 2.5), r * cos(t * 2.5)});
        y[ix] = (uint8_t)class_number;
    }

    return std::make_pair(X, y);
}