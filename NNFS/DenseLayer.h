#pragma once
#include "Tensor.h"

template<typename T, std::size_t n_inputs, std::size_t n_neurons>
class LayerDense
{
	Tensor<T, n_neurons, n_inputs> weights;
	Tensor<T, n_neurons> biases;
public:
	LayerDense()
	{
		weights = randn<T, n_neurons, n_inputs>() * 0.01;
		biases = zeros<T, n_neurons>();
	}

	Tensor<T, n_neurons> Forward(Tensor<T, n_inputs> input)
	{
		return dot(input, weights) + biases;
	}

	template<std::size_t samples>
	Tensor<T, samples, n_neurons> Forward(Tensor<T, samples, n_inputs> inputs)
	{
		auto temp = dot(inputs, weights);
		return temp + biases;
	};

};